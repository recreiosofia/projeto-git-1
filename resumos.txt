git init: Inicia um novo repositório Git, criando um diretório .git para armazenar metadados e configurações do repositório.

git status: Exibe o estado atual do diretório de trabalho, mostrando arquivos modificados, adicionados ou não rastreados.

git add arquivo/diretório: Adiciona um arquivo ou diretório específico à área de preparação (staging).

git commit -m "Primeiro commit": Registra as mudanças na área de preparação no repositório, criando um novo commit com uma mensagem descritiva.

git log: Exibe o histórico de commits do ramo atual.

git log arquivo: Mostra o histórico de commits específico para um arquivo.

git reflog: Exibe um registro de todas as operações de referência (HEAD) realizadas, útil para recuperar commits perdidos.

git show: Exibe informações sobre o último commit, incluindo detalhes sobre as alterações.

git show <commit>: Exibe informações sobre um commit específico.

git diff: Mostra as diferenças entre o diretório de trabalho e a área de preparação.

git diff <commit1> <commit2>: Exibe as diferenças entre dois commits.

git reset --hard <commit>: Reseta o HEAD e a área de preparação para um commit específico, descartando todas as alterações não commitadas.

git branch: Lista todas as branches locais e destaca a branch atual.

git branch -r: Lista as branches remotas.

git branch -a: Lista todas as branches locais e remotas.

git branch -d <branch_name>: Deleta a branch local especificada.

git branch -D <branch_name>: Força a exclusão de uma branch local, mesmo se não estiver completamente mesclada.

git branch -m <nome_novo>: Renomeia a branch local atual.

git branch -m <nome_antigo> <nome_novo>: Renomeia uma branch local específica.

git checkout <branch_name>: Muda para a branch especificada.

git checkout -b <branch_name>: Cria e muda para uma nova branch.

git clone: Clona um repositório Git existente para o diretório local.

git pull: Atualiza o repositório local com as mudanças do repositório remoto.

git push: Envia as mudanças locais para o repositório remoto.

git remote -v: Exibe os URLs remotos associados ao repositório.

git remote add origin <url>: Adiciona um repositório remoto chamado "origin" com o URL especificado.